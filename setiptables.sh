#!/bin/bash

# Add IP addresses to iptables 'zapret' table
echo "Flushing iptables rules"
iptables -F zapret

# Save current iptables rules to a file
iptables-save -t filter | head -n -2 > iptables-save.txt

echo "Reading iplist.txt"
while read line
do
	#iptables -A zapret -d "$line" -j ACCEPT
	echo -A zapret -d "$line" -j ACCEPT >> iptables-save.txt
done < iplist_collapsed_prefix.txt
echo COMMIT >> iptables-save.txt

# "Restore" iptables with filled zapret table
iptables-restore < iptables-save.txt
echo "Done!"

